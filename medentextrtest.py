import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import plac
import spacy
from spacy.gold import GoldParse
from spacy.util import minibatch, compounding
from spacy.scorer import Scorer
import configparser
import re


def evaluate(medentextr_model, examples):
    print('examples', examples)
    scorer = Scorer()
    for input_, annot in examples:
        doc_gold_text = medentextr_model.make_doc(input_)
        annot = eval(annot)
        gold = GoldParse(doc_gold_text, entities=annot['entities'])
        pred_value = medentextr_model(input_)
        scorer.score(pred_value, gold)
    MU = []
    MC = []
    print(scorer.scores)
    for key in scorer.scores['ents_per_type']:
        if key == 'MEDUNIT':
            MU.append(scorer.scores['ents_per_type'][key]['p'])
            MU.append(scorer.scores['ents_per_type'][key]['r'])
            MU.append(scorer.scores['ents_per_type'][key]['f'])
        if key == 'MEDCONCEPT':
            MC.append(scorer.scores['ents_per_type'][key]['p'])
            MC.append(scorer.scores['ents_per_type'][key]['r'])
            MC.append(scorer.scores['ents_per_type'][key]['f'])

    Labels = ["Precision", "Recall", "F-measure"]
    y_pos = np.arange(len(Labels))
    fig = plt.figure()
    fig.patch.set_facecolor('Beige')
    ax = plt.axes()
    ax.set_facecolor('Bisque')
    plt.bar(y_pos + 0, MU, width=0.1, color='Coral', label='MEDUNIT')
    plt.bar(y_pos + 0.4, MC, width=0.1, color='LightGreen', label='MEDCONCEPT')
    plt.xticks(y_pos, Labels)
    plt.legend(('MEDUNIT', 'MEDCONCEPT'))
    plt.ylabel('Performance')
    plt.xlabel('Parameters (P,R,F)')
    plt.title("Performances for new named entities")
    plt.show()
    return scorer.scores


def main():
    # test the trained model

    nlp_model = spacy.load('C:/models')
    print(nlp_model.pipe_labels['ner'])
    xl = pd.ExcelFile('testdata.xlsx')
    dfs = {}
    for sheet_name in xl.sheet_names:
        if sheet_name == 'Sheet1':
            dfs['cstmr'] = xl.parse(sheet_name)
    test_data = dfs['cstmr'].values.tolist()
    evaluate(nlp_model, test_data)


if __name__ == "__main__":
    plac.call(main())
